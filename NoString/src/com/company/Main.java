package com.company;

/*
* Раздел: Warmup-1
* Задача: notString
* */
public class Main {

    public static void main(String[] args) {

    }
    public String notString(String str) {
        String front =  "not";

        if (str == front) {
            return str;
        } else if (str.length() < 4) {
            return front + " " + str;
        } else if (str.substring(0, 3).equals(front)) {
            return str;
        }
        else{
            return front +" "+ str;
        }
    }
}
