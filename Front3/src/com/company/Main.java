package com.company;

/*
* Раздел: Warmup-1!
* Задача: front3!
*/
public class Main {

    public static void main(String[] args) {
        // write your code here
    }

    public String front3(String str) {
        String word;

        if (str.length() < 3) {
            return str + str + str;
        } else {
            word = str.substring(0, 3);
            return word + word + word;
        }
    }
}
