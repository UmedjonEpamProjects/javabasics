package com.company;

/*
*Раздел: Warmup-1!
*Задача: Front22!
*/
public class Main {

    public static void main(String[] args) {

    }
    public String front22(String str) {
        String part;

        if (str.length() <= 2) {
            return str + str + str;
        } else {
            part = str.substring(0, 2);
            return part + str + part;
        }
    }
}
