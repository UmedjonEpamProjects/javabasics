package com.company;

/*
* Раздел: Warmup-1!
* Задача: frontBack!
*/
public class Main {

    public static void main(String[] args) {

    }
    public String frontBack(String str) {
        String firstpiece;
        String middlepiece;
        String lastpiece;

        if (str.length() >= 2) {
            firstpiece = str.substring(0,1);
            middlepiece = str.substring(1, str.length() - 1);
            lastpiece = str.substring(str.length()-1);

            return lastpiece + middlepiece + firstpiece;
        } else {
            return str;
        }
    }
}
